### Mobilize Framework ###

To set-up git keys do this :
http://serverfault.com/questions/194567/how-do-i-tell-git-for-windows-where-to-find-my-private-rsa-key


A mobile focussed, framework built in AngularJs for eLearning content presentation.

```
git clone <repository url>
git clone git@bitbucket.org:savveproducts/mobilize.git
```
to get the repository




Do a global installation and not a local one just to meet dependencies for future projects
To compile the framework you need the following pre-installed
* NodeJs / npm
* bower (npm install -g bower)
* gulp  (npm install -g gulp)
* grunt (npm install -g grunt)

Package.json file has been set up to install all required dev dependencies. In case of adding any more dependencies 
```
npm install 
```
will install all required development dependencies


In case of installing any bower modules,
```
bower install 
```
By default, running npm will install all bower modules as well.


#Folder structure
 - bower_components
 - config (configurations for gulp and npm)
 - js (dev files for js)
 - html (templates for dev)
 - scss (dev sass files)
 - node_modules (npm installed modules)
 - dist : This will be final deliverable. All the templates and html goes in here and this will be the file being packaged

To run a local server:
```
npm start
```
This will run a local webserver on your system. You can now access the files through the address
```
http://localhost:8000/dist/index.html
```



#GULP USAGE
Gulp, is used for running any associated tasks required for setting up the modules. 
By default, running the gulp command will compile any sass files in the sass folder and also combine all the javascript files to a bundle.
```
gulp 0
```

Here is an non-exhaustive list of all gulp commands which are part of the framework
```
gulp //This will run the default task => build
gulp build       // Use this to create a dev version  [sub-tasks : bundle , sass ]
gulp make        // Use this to create a release version  [sub-tasks : bundle , sass , version]
gulp external    //Will combine all vendor files into 1 file
gulp watch       //Will initialise an watch on all sass files and js files
gulp bundle      //This will create an js bundle

//Other tasks
gulp validate // Runs jshint , ddescribe-iit and karma and validates the code.
gulp sass  //This is used to compile the sass files to create a css file (mobilize.css)
```

to feed parameters to gulp use - notation (minimist)
```
gulp -r 1
```
will create the minified files and release the project
```
gulp project -p Projectname
```
will create a new project from the framework

More about the sub tasks :
* The gulp sass command will compile the sass files and create a css file. If the build config is set to release then the css is minified and an additional file is created
* Bundle command is a collection of 4 tasks : scripts , scripts-ng , vendor , version
* Bundle command will also create a mobile.bundle.js. To do this, it will use the  following
    - buildConfig.bundleBanner
    - buildConfig.mobilizeBundleFiles
* scripts command will run through the mobilizeFiles and create a mobilize.js file.
* scripts-ng will run through the angularMobilizeFiles and create a mobilize-angular.js
* vendor - for a file to be used copy the file to config/lib/js.
*        - you and additionally add eot/svg/ttf/woff font files in this folder and this will be compiled as well

-------------