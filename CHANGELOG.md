### Mobilize Framework ###
### 1.0.1 "yomoto"
### RELEASED :Development

#### Updates
* **checkbox: @since 1.0.0**

#### Bug Fixes

####You can view



### 1.0.0 "xenifer"
### RELEASED :08-12-2014

#### Updates
* **checkbox:**
    * Created checkbox manager
    * Fixed to allow the dynamic loading through templates and static constant loading
    * Evaluation and reset functionalities
    * Button functionality on demand
    * Unit testing for the checkbox functionality added.[UNIT](unit/?at=1.0.0)

#### Bug Fixes
* **gulpfile.js:**
    * Fixed gulp release and config

####You can view
*  the current licence here : [LICENSE](License.md?at=1.0.0)
*  the current release name : [RELEASE NAMES ](ReleaseNames.info?at=1.0.0)
*  the proposed roadmap     : [RoadMap](docs/roadmap.md?at=1.0.0)
*  the todo list            : [Todo](docs/todo.md?at=1.0.0)



### 0.0.5 "xenifer"
### Updates
* **gulpfile.js:**
        * Set up bower / node / gulp
        * Updated all gulp functionalities

### OLDER VERSIONS ###
DEV LOGS NOT AVAILABLE
