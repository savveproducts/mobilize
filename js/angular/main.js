window.MobilizeModule = angular.module('mobilize', ['ionic','ngAnimate', 'ngSanitize', 'ui.router']),
    extend = angular.extend,
    forEach = angular.forEach,
    isDefined = angular.isDefined,
    isNumber = angular.isNumber,
    isString = angular.isString,
    jqLite = angular.element;
