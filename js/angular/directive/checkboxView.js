/**
 * @ngdoc directive
 * @name mobilizeCheckbox
 * @module mobilize
 * @delegate mobilize.service.checkboxDelegate
 * @restrict 'AEC'
 * @controller mobilize.controller.checkboxController
 * @description
 * Creates a checkbox manager for controlling a collection of checkbox
 *
 * Events::
 *  The following events will be emited by the checkbox to allow for handling
 *
 *  $broadcast -- dispatches the event downwards to all child scopes,
 *      --NONE--
 *  $emit -- dispatches the event upwards through the scope hierarchy.
 *      $mobilizeCheckbox.evaluated : Emitted when the mobilize Checkbox is evaluated .
 *           This has two parameters
 *              @param {array=}    $resultStats : Provides a processed result list statistics
 *              @param {array=}    $result      : Exposes the items as objects .
 *     $mobilizeCheckbox.destroyed   : Emmitted when the mobilize Checkbox is destroyed
 *     $mobilizeCheckbox.reset       : Emmitted when the mobilize Checkbox is destroyed
 *
 *
 * @usage
 * ```
         <mobilize-checkbox
         mobi-items="{{checkboxItems}}"
         mobi-randomize="{{checkboxRandomize || false}}"
         mobi-title="{{checkboxTitle || 'blah blah blah TITLE COMES HERE '}}"
         mobi-sub-title="{{checkboxSubTitle || 'blah blah blah SUB TITLE COMES HERE '}}"
         mobi-header-content="{{checkboxHeaderContent || 'blah blah blah'}}"
         mobi-tpl="partial/partial-checkbox.html"
         mobi-answer-highlights="false"
         delegate-handle="cb-1">

         <div class="addl-content">
         <span class='caf-content' ng-if="$resultStats.num_invalid == 0 && $resultStats.evaluated">
         SOME CAF CONTENT COMES HERE
         </span>
         <span class='waf-content' ng-if="$resultStats.num_invalid > 0 && $resultStats.evaluated">
         SOME WAF CONTENT COMES HERE
         </span>
         </div>
         </mobilize-checkbox>
 *```
 * Js:
 * ```
         mymodule.controller('AppCtrl', ['$scope', function ($scope, $mobilizeCheckbox) {
                $scope.checkboxItems = [
                    {title: 'Item 1 TRUE YES ', verifiedIf: true},
                    {title: 'Item 2 TRUE No ', verifiedIf: false},
                    {title: 'Item 3 TRUE No ', verifiedIf: false},
                    {title: 'Item 4 TRUE No ', verifiedIf: false},
                    {title: 'Item 5 TRUE YES ', verifiedIf: true},
                    {title: 'Item 6 TRUE YES ', verifiedIf: true},
                ];
                $scope.checkboxHeaderContent = "<p>SOME HEADER CONTENT COMES HERE </p> <b>HEADER CONTENT CAN HAVE TITLES TOO</b> <h4>AND NICE FORMATTING USING HTML TAGS</h4>";
            }])
 * ```
 *
 *
 * Note :
 * To keep it clean and consise and also to make sure the directive does not break during compilation
 * please add any additional content into the <div class='addl-content'></div>
 * Content inside the div will be compiled into the directive. So variables can be used in inside as shown in the example
 *
 *  @param {string=}            mobi-items A json string of items .
 *  @param {boolean=}           mobi-randomize Randomize the checkbox on load
 *  @param {string=}            mobi-title Title field on the partial
 *  @param {string=}            mobi-sub-title sub title field on the partial
 *  @param {string=}            mobi-header-content  Header content on the partial
 *  @param {string=} delegate-handle The handle used to identify this checkboxView with {@link mobilize.service::$mobilizeCheckboxDelegate}
 *  @param {optional string=}   mobi-tpl  If a template is not provided the default structure will be loaded. This is a useful option to customize the checkbox to requirements
 *  @param {optional string=}   mobi-prevent-default-buttons This will prevent angular from loading the default button if you choose to replace the buttons in the addl-content
 *
 */
(function (MobilizeModule, mobilize, angular) {

    function toBoolean(value) {
        if (value && value.length !== 0) {
            var v = ("" + value).toLowerCase();
            value = !(v == 'f' || v == '0' || v == 'false' || v == 'no' || v == 'n' || v == '[]');
        } else {
            value = false;
        }
        return value;
    }




    MobilizeModule
        .directive('mobilizeCheckbox', [
            '$controller',
            '$timeout',
            '$ionicBind',
            '$http',
            '$templateCache',
            '$compile',
            '$mobilizeTPL',
            function ($controller, $timeout, $ionicBind, $http, $templateCache, $compile,$mobilizeTPL) {
                return {
                    restrict: 'AEC',
                    transclude: true,
                    scope: true,
                    controller: function () {
                    },
                    compile: function (element, attr, transclude) {
                        var innerElement;
                        //set up the manager
                        element.addClass('checkbox-manager');
                        return {pre: preLink};


                        /**
                         * Prelink functionality on compile
                         * @param $scope
                         * @param $element
                         * @param $attr
                         */
                        function preLink($scope, $element, $attr) {
                            var checkboxCtrl, checkboxView;
                            var buttons = [];

                            $ionicBind($scope, $attr, {
                                mobiTitle: '@',
                                mobiSubTitle: '@',
                                mobiHeaderContent: '@'
                            });


                            //Make sure the items get updated every time there is an update
                            $attr.$observe('mobiItems', function (items) {
                                checkboxCtrl.setItems(items);
                            });
                            $attr.$observe('mobiRandomize', function (randomize) {
                                checkboxCtrl.setRandomize(randomize);
                            });
                            $attr.$observe('mobiTitle', function (title) {
                                checkboxCtrl.setTitle(title);
                            })
                            $attr.$observe('mobiSubTitle', function (title) {
                                checkboxCtrl.setSubTitle(title);
                            })
                            $attr.$observe('mobiHeaderContent', function (title) {
                                checkboxCtrl.setHeaderContent(title);
                            })
                            $attr.$observe('mobiType', function (type) {
                                checkboxCtrl.setType(type);
                            })
							
                            transclude($scope, function (clone) {
                                children = clone;
                            });


                            //Load the html from a tpl or from the default template
                            if (isDefined($attr.mobiTpl)) {
                                //A template has been defined. Load  the template from there
                                $http.get($attr.mobiTpl, {cache: $templateCache})
                                    .success(function (html) {
                                        innerElement = jqLite(html);
                                        innerElement.append(element.contents());

                                        element
                                            .append($compile(innerElement.html())($scope))
                                            .append(children);

                                    });

                            }
                            else {
                                //Load it from the default
                                innerElement = jqLite($mobilizeTPL.checkboxDefaultTpl);
                                innerElement.append(element.contents());

                                element
                                    .append($compile(innerElement.html())($scope))
                                    .append(children);
                            }


                            if ((isDefined($attr.mobiPreventDefaultButtons) && !$attr.mobiPreventDefaultButtons) || !isDefined($attr.mobiPreventDefaultButtons)) {
                                buttons = [
                                    {
                                        name: 'reset',
                                        text: 'Reset',
                                        type: 'button-default',
                                        onTap: 'reset',
                                        showIf: true
                                    },
                                    {
                                        name: 'submit',
                                        text: 'Submit',
                                        type: 'button-default',
                                        onTap: 'evaluate',
                                        disabledIf: "!(items | objectkeyValid:'checked':true) || $resultStats.evaluated"
                                    }
                                ]
                            }
                            //Set up watches on the variables here
                            var checkboxOptions = {
                                el: $element[0],
                                delegateHandle: $attr.delegateHandle,
                                items: $attr.mobiItems,
                                title: $attr.mobiTitle,
                                subTitle: $attr.mobiSubTitle || null,
                                randomize: toBoolean($attr.mobiRandomize),
                                buttons: buttons
                            };


                            //Set up the controller for checkboxes
                            checkboxCtrl = $controller('$mobilizeCheckbox', {
                                $scope: $scope,
                                checkboxOptions: checkboxOptions
                            });

                            checkboxCtrl.initialize();


                            //Ability to reset the checkbox items on the controller. (to allow for randomize)
                            $scope._checkboxView = $scope.$parent.checkboxView = checkboxCtrl.checkboxView;

                            //For testing
                            $scope._checkboxCtrl = checkboxCtrl;
                        }
                    }
                };
            }]);
}(window.MobilizeModule, window.mobilize, window.angular));