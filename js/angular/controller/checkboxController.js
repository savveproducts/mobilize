/**
 * @private
 */
(function (MobilizeModule) {
    var shuffleArray = function (array) {
        var m = array.length, t, i;

        // While there remain elements to shuffle
        while (m) {
            // Pick a remaining element…
            i = Math.floor(Math.random() * m--);

            // And swap it with the current element.
            t = array[m];
            array[m] = array[i];
            array[i] = t;
        }

        return array;
    }

    MobilizeModule.controller('$mobilizeCheckbox', [
        '$scope',
        'checkboxOptions',
        '$mobilizeCheckboxDelegate',
        '$timeout',
        '$window',
        '$location',
        '$document',
        function ($scope, checkboxOptions, $mobilizeCheckboxDelegate, $timeout, $window, $location, $document) {
            //Set up
            var self = this;
            //testing
            self.checkboxOptions = checkboxOptions;

            var scope = self.$scope = $scope;
            var element = checkboxOptions.el;
            var $element = self.$element = jqLite(element);
            var items;
            var $responses = $scope.$responses = [];
			var type = self.type = self.$scope.mobiType = "single";
			
			
            var initialized = false;

            $scope.$result = {
                valid:[],
                invalid : [],
                neutral : [],
                pristine: []
            };

            $scope.$resultStats = {
                num_valid : 0,
                num_invalid : 0,
                num_neutral : 0,
                num_pristine : 0,
                evaluated : false
            };




            //Create the checkbox view
            var checkboxView = self.checkboxView = new mobilize.views.Checkbox(checkboxOptions);

            //Bind the self to the element as a controller so other directives can require this controller
            ($element.parent().length ? $element.parent() : $element).data('$$mobilizeCheckboxController', self);

            //Deregister the instance from the delegate
            var deregisterInstance = $mobilizeCheckboxDelegate.
                _registerInstance(self, checkboxOptions.delegateHandle, function () {
                    return true
                });
				
			
            /**
             * Initialize the check box controller.
             * @param options
             * @returns {boolean}
             */
            self.initialize = function (options) {

                if(self.initialized){
                    return false;
                }


                self.initialized = true;
                var options=angular.extend({},checkboxOptions,(options||{}));
                if (!options) {
                    return false;
                }


                self.setRandomize(options.randomize || false)
                self.setButtons(options.buttons||[]);

                if (isDefined(options.items)) {
                    //items is defined
                    self.setItems(options.items);
                }
                //By default all the current status are unknown
                $scope.$result.pristine=items;
            }

            /**
             * Set the visible buttons to show. This is defined in the directives and passed through
             *```
             *   buttons :[
             *      {
             *          text:'Ok',
             *          type:'button-default',
             *          onTap:functionNameOnTheScope
             *      },
             *      {
             *          text:'Submit',
             *          type:'button-default',
             *          onTap:functionNameOnTheScope
             *      },
             *      ]
             * }
             * ```
             * @param buttons
             * @returns {*}
             */
            self.setButtons= function(buttons){
                buttons=buttons||[];
                $scope.buttons=buttons;
                return self;
            }
			/**
             * Set up whether you can select only one checkbox or multiple checkboxes
             * @param objectType "single" or "multiple"
             */
			self.setType = function(objectType){
				type = self.type = self.$scope.mobiType = objectType;
				return self;
			}

            /**
             * Randomize the checkboxes or not
             * @param randomize
             */
            self.setRandomize = function (randomize) {
                self.randomize = randomize || false;
                return self;
            }
            /**
             * Set up the items in the checkbox
             * @param its
             */
            self.setItems = function (its) {
                if (angular.isString(its)) {
                    items = angular.fromJson(its);
                }
                else {
                    items = its;
                }

                self.items = $scope.items = items;

                //Randomize the checkboxes
                if(self.randomize){
                    shuffleArray(self.items);
                }
				
				angular.forEach(self.items, function(item, key) {
					item.checked = item.checked || false;
				});
				
				
                return self;
            }
            /**
             * Set up a title of the check box
             * @param title
             * @returns {*}
             */
            self.setTitle = function (title) {
                $scope.title = title;
                return self;
            }

            /**
             * Set up sub title
             * @param subTitle
             * @returns {*}
             */
            self.setSubTitle = function (subTitle) {
                $scope.subTitle = subTitle;
                return self;
            }

            /**
             * Set content in the header
             * @param headerContent
             * @returns {*}
             */
            self.setHeaderContent = function (headerContent) {
                $scope.headerContent = headerContent;
                return self;
            }

            /**
             * Returns the check box view object
             * @returns {*}
             */
            self.getCheckboxView = function () {
                return self.checkboxView;
            }



            /**
             * Calculate stats for the check box
             */
            self.calculateStats = function(){
                $scope.$resultStats.num_valid = ($scope.$result.valid &&  $scope.$result.valid.length);
                $scope.$resultStats.num_invalid = ($scope.$result.invalid &&  $scope.$result.invalid.length);
                $scope.$resultStats.num_neutral = ($scope.$result.neutral &&  $scope.$result.neutral.length);
                $scope.$resultStats.num_pristine = ($scope.$result.pristine &&  $scope.$result.pristine.length);

                $scope.$emit('$mobilizeCheckbox.evaluated',{'$resultStats':$scope.$resultStats, '$result':$scope.$result})
            }



            /**
             * Watch the result variable to calculate the stats
             */
            $scope.$watch('$result',function(){
                self.calculateStats();
			});
            /**
             * View functionalities
             * @param items
             */
            $scope.evaluate = function(items , reset ){
                reset = reset || false;
                var valid=[]
                    ,invalid=[]
                    ,neutral=[]
                    ,pristine=[];

                $scope.$resultStats.evaluated = true;


                angular.forEach(items, function(item, key) {
                    item.ngClass= [];

                    //Mark the right and wrong answers
                    if(item.verifiedIf){
                        item.ngClass.push('mobi-correct');
                    }
                    else{
                        item.ngClass.push('mobi-incorrect');
                    }


                    //Mark whether the checkbox has been clicked before
                    if(!isDefined(item.checked)){
                        pristine.push(item);
                        item.ngClass.push('mobi-pristine');
                    }

                    //if we do not know whether the item has a verification at all
                    if(!isDefined(item.verifiedIf)){
                        neutral.push(item);
                        item.ngClass.push('mobi-neutral');
                        return true;
                    }
                    if((isDefined(item.checked) && item.checked) && item.verifiedIf){
                        valid.push(item);
                        item.ngClass.push('mobi-valid');
                        return true;
                    }

                    if(!item.verifiedIf && (!isDefined(item.checked) || !item.checked)){
                        valid.push(item);
                        item.ngClass.push('mobi-valid');
                        return true;
                    }

                    item.ngClass.push('mobi-invalid');
                    invalid.push(item);
                    return true;
                });


                $scope.$result = {
                    valid: valid,
                    invalid : invalid,
                    neutral : neutral,
                    pristine: pristine
                };


            }


            /**
             * Evaluate from the values in the root scope
             */
            $scope.evaluateFromSelf = function(){
                return $scope.evaluate($scope.items);
            }

            /**
             * Reset the check boxes
             * @param items
             */
            $scope.reset = function(items){
                angular.forEach(items, function(item, key) {
                    item.checked=false;
                });

                $scope.$result = {
                    valid: [],
                    invalid : [],
                    neutral : [],
                    pristine: $scope.items
                };
                $scope.$resultStats.evaluated = false;
                $scope.$emit('$mobilizeCheckbox.reset');
            }


            $scope.$on('$destroy', function () {
                //Stop the watch
                $scope.$watch('$result',angular.noop);

                self.$scope = null;
                if (self.items) {
                    self.items = null;
                }

                if (self.content) {
                    self.content.element = null;
                    self.content = null;
                }
                $scope.$emit('$mobilizeCheckbox.destroyed');
            });


            //Private functions
            /**
             * Shuffle up the options
             */
            $scope.shuffleOptions = function(){
                shuffleArray(self.items);
            }

            //For testing
            $scope._calculateStats  = self.calculateStats;
        }
    ]);


}(window.MobilizeModule));