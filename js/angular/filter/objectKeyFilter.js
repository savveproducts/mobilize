(function (MobilizeModule,mobilize) {
    /**
     * @ngdoc filter
     * @name objectkey
     * @module mobilize
     * @delegate
     *
     *
     * @description
     * Filter an array of objects by key
     * Works very similiar to the native angular JS Filtering ( friend in friends | filter: { name: query, age: '20' })
     *
     * Basic usage:
     * ```
     *  <p> {{"items | objectkey:'checked':true" }}</p>
     * ```
     * Filter can also be accessed through the $filter service like so
     * ```
     * $filter('objectkey')($scope.items,'checked',true)
     * ```
     * @param {array=} An array of objects fed in as items
     * @param {string=} The key of each object within the array to compare each element to
     * @param {bool | string =} the value to which to compare the string to
     */
    MobilizeModule.filter(
        'objectkey',
        function(){
            return function (items, key,value) {
                var filtered = [];
                forEach(items,function(it){
                    if(it[key]==value){
                        filtered.push(it);
                    }
                });
                return filtered;
            };
        }
    );
}(window.MobilizeModule,window.mobilize));
