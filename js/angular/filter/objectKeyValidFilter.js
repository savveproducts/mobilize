(function (MobilizeModule, mobilize) {
    /**
     * @ngdoc filter
     * @name objectkey
     * @module mobilize
     * @delegate
     *
     *
     * @description
     * Loops through an array of objects and returns true if atleast one of the object in the array has a valid key
     * More useful if you are using this with ng-Show , ng-If, ng-hide and the like
     *
     *
     * Basic usage:
     * ```
     *  <p> {{"items | objectkey:'checked':true" }}</p>
     * ```
     * Filters can also be accessed through the $filter service like so
     * ```
     * $filter('objectkeyValid')($scope.items,'checked',true)
     * ```
     * @param {array=} An array of objects fed in as items
     * @param {string=} The key of each object within the array to compare each element to
     * @param {bool | string =} the value to which to compare the string to
     */
    MobilizeModule.filter(
        'objectkeyValid',
        function () {
            return function (items, key, value) {
                if (angular.isArray(items)) {

                    var valid = false;
                    forEach(items, function (it) {
                        if(!valid){
                            valid = (isDefined(it[key]) && (it[key] == value));
                        }
                    });

                    return valid;
                }
            };
        }
    );
}(window.MobilizeModule, window.mobilize));
