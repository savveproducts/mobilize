(function (MobilizeModule, mobilize, angular) {
    'use strict';

    MobilizeModule.constant('$mobilizeTPL',{
        checkboxDefaultTpl : '' +
            '<div class="checkbox-wrapper">' +
                '<div class="checkbox-header">' +
                    '<h3 class="checkbox-title" ng-bind-html="title" ng-if="title"></h3>'+
                    '<h4 class="checkbox-title" ng-bind-html="subTitle" ng-if="subTitle"></h4>'+

                    '<p class="checkbox-header-content" ng-bind-html="headerContent" ng-if="headerContent"></p>'+
                '</div>'+
                '<ul class="checkbox-container">'+
                    '<li class="item item-checkbox">'+
                        '<ion-checkbox ng-repeat="item in items" '+
                        'ng-model="item.checked" '+
                        'ng-checked="item.checked || false" '+
                        'ng-class="item.ngClass" '+
                        'ng-disabled="$resultStats.evaluated" '+
                        '> '+
                            '<span class="item-title" translate>{{ item.title }}</span> '+
                        '</ion-checkbox> '+
                    '</li> '+
                '</ul> '+

                '<div class="checkbox-footer" ng-show="buttons.length"> '+
                    '<button ng-click="{{button.onTap}}(items,button,$event)" '+
                    'ng-repeat="button in buttons" '+
                    'class="button" ' +
                    'name="{{button.name}}"   '+
                    'ng-class="button.type || button-default" '+
                    'ng-bind-html="button.text" '+
                    'ng-show="{{button.showIf || true}}" '+
                    '> '+
                    '</button> '+
                '</div> '+
            '</div>'
    });

})(window.MobilizeModule,window.mobilize,window.angular);