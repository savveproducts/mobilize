(function (MobilizeModule,mobilize) {
    /**
     * @ngdoc service
     * @name $mobilizeCheckboxDelegate
     * @module mobilize
     *
     * @description
     * Delegate for controlling the {@link mobilize.directive:checkboxView} directive
     *
     * Methods called directly on the $mobilizeCheckboxDelegate service will control all $mobilizeCheckbox
     * directives. Use the {@link mobilize.service:$mobilizeCheckboxDelegate#$getByHandle $getByHandle}
     * to control specific instances
     *
     * @usage
     * ```
     *
     * ```
     */
    MobilizeModule.service('$mobilizeCheckboxDelegate',mobilize.delegateService([
        /**
         * @ngdoc method
         * @name $ionicTabsDelegate#$getByHandle
         * @param {string} handle
         * @returns `delegateInstance` A delegate instance that controls only the
         * {@link ionic.directive:ionTabs} directives with `delegate-handle` matching
         * the given handle.
         *
         * Example: `$ionicTabsDelegate.$getByHandle('my-handle').select(0);`
         */
    ]));
}(window.MobilizeModule,window.mobilize));
