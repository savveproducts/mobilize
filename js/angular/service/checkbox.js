(function (MobilizeModule) {

    /**
     * @author sundar
     * @name $mobilizeCheckbox
     * @module mobilize
     * @since 0.4
     * @copyright  Copyright (c) 2014 Savv-e Pty Ltd
     * Related: {@link mobilize.controller:mobilizeCheckbox mobilizeCheckbox controller}.
     *
     *
     * The Checkbox is collection of checkbox pre-configured with all common functionalities in eLearning.
     *
     * **Notes:**
     *  -Events being broadCasted :
     *
     *
     *  @usage:
     *
     *
     */


    MobilizeModule
        .factory('$mobilizeCheckbox', [
            '$rootScope',
            '$ionicBody',
            '$compile',
            '$timeout',
            '$q',
            '$log',
            function ($scope, $ionicBody, $compile, $timeout, $q, $log) {
                var config = {};
                var checkboxStack = [];
                var $mobilizeCheckbox = {
                    /**
                     * @ngdoc method
                     * @description
                     * Show a list of checkboxes . The master controller will manage the collection of checkboxes
                     *
                     * @name $mobilizeCheckbox#show
                     * @param {object} options : The options for the new checkbox manager of the form
                     *
                     * ```
                     * {
                     *  title: '', //The title of the checkboxes
                     *  template : '', //String(optional) The html template to create the checkboxes
                     *  templateUrl: '', //String(optional) The URL of an html template to place the checkboxes in
                     *  scope : null, //Scope(optional) The scope to link to the popup content.
                     *  buttons :[
                     *      {
                     *          text:'Clear All',
                     *          type:'button-default',
                     *          onTap:function(e){
                     *              //e.preventDefault() will stop the checkbox from being cleared on tap
                     *          }
                     *      },
                     *      {
                     *          text:'Submit',
                     *          type:'button-default',
                     *          onTap:function(e){
                     *              //e.preventDefault() will stop the checkbox from being evaluated
                     *              return scope.data.response;
                     *          }
                     *      },
                     *  ]
                     * }
                     * ```
                     * @returns {object} A promise which is resolved when the checkboxes are evaluated.
                     */

                    show:function(){}, //showCheckbox,

                    /**
                     * @ngdoc method
                     * @name @mobilizeCheckbox#alert
                     * @description Show a alert popup with a message  that the user can tap to close
                     * ```
                     * {
                     *      title:'',//Title of the alert message
                     *      subTitle:'',//Sub-title of the alert message
                     *      template:'',//String(optional) The html template to place in the popup body
                     *      templateUrl:'',//String(optional) The URL of an html template to place in the popup body,
                     *      buttons :[
                     *      {
                     *          text:'Ok',
                     *          type:'button-default',
                     *          onTap:function(e){
                     *              //Any additional functionalities you need on the tap
                     *          }
                     *      },
                     *      {
                     *          text:'Submit',
                     *          type:'button-default',
                     *          onTap:function(e){
                     *              //Any additional functionalities you need on the tap
                     *          }
                     *      },
                     *      ]
                     * }
                     * ```
                     */
                    alert: function(){}, //showAlert,

                    /**
                     * @ngdoc method
                     * @name $mobilizeCheckbox#confirm
                     * @description
                     * Show a simple confirm popup with a Cancel and OK button.
                     *
                     * Resolves the promise with true if the user presses the OK button, and false if the
                     * user presses the Cancel button.
                     *
                     * @param {object} options The options for showing the confirm popup, of the form:
                     *
                     * ```
                     * {
                     *   title: '', // String. The title of the popup.
                     *   subTitle: '', // String (optional). The sub-title of the popup.
                     *   template: '', // String (optional). The html template to place in the popup body.
                     *   templateUrl: '', // String (optional). The URL of an html template to place in the popup   body.
                     *   cancelText: '', // String (default: 'Cancel'). The text of the Cancel button.
                     *   cancelType: '', // String (default: 'button-default'). The type of the Cancel button.
                     *   buttons :[
                     *      {
                     *          text:'Ok',
                     *          type:'button-default',
                     *          onTap:function(e){
                     *              //Any additional functionalities you need on the tap
                     *          }
                     *      },
                     *      {
                     *          text:'Submit',
                     *          type:'button-default',
                     *          onTap:function(e){
                     *              //Any additional functionalities you need on the tap
                     *          }
                     *      },
                     *      ]
                     * }
                     * ```
                     *
                     * @returns {object} A promise which is resolved when the popup is closed. Has one additional
                     * function `close`, which can be called with any value to programmatically close the popup
                     * with the given value.
                     */
                    confirm:function(){}, //showConfirm,

                    /**
                     * @private For testing
                     */
                    _createCheckbox: function(){} //createCheckbox
                };


                return $mobilizeCheckbox;


                var showCheckbox=function(){}
                var showConfirm=function(){}
                var showAlert=function(){}
                var createCheckbox=function(){}
                /**
                 * @ngdoc Controller
                 * @name mobilizeCheckbox
                 * @module mobilize
                 * @description
                 * Instantiated by the {@link mobilize.service::$mobilizeModal} service
                 *
                 * Be sure to call remove when done with the check box to clean up and avoid memory leaks
                 *
                 * Events being broadcasted ::
                 *
                 */
                var CheckboxView = mobilize.views.Checkbox.inherit({
                    /**
                     * @ngdoc method
                     * @name mobilizeCheckbox#initialize
                     * @description Creats a checkbox controller instance
                     * @param options
                     */
                    initialize: function (options) {
                        mobilize.views.Checkbox.prototype.initialize.call(this, options);
                        this.animation = options.animation || 'fade-in';
                    },

                    show: function (target) {
                        var self = this;
                        if (self.scope.$$destroyed) {
                            $log.error('Cannot call ' + self.viewType + '.show() after remove(). Please create a new ' + self.viewType + ' instance.');
                            return;
                        }

                        var checkboxEl = jqLite(self.modalEl);

                        //Handle other show functionalities here
                    },

                    hide: function () {
                        //Handle hide functionalities
                    },
                    remove: function () {
                        //handle remove functionalities
                    }
                });


            }]);

}(window.MobilizeModule));
