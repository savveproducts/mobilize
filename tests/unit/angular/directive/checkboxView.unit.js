describe('$mobilizeCheckbox directive', function () {
    var el, rootScope, compile;

    var html = {
        base: '' +
            '<mobilize-checkbox mobi-items="{{items}}" mobi-randomize="{{randomize||false}}"' +
            ' mobi-randomize="{{randomize || false}}"' +
            ' mobi-title="{{title}}"' +
            '></mobilize-checkbox>'
    };


    beforeEach(module('mobilize'));

    beforeEach(inject(function ($controller, $timeout, $ionicBind, $http, $templateCache, $compile, $mobilizeTPL, $rootScope) {
        rootScope = $rootScope;
        compile = $compile;
    }));



    it('should load items', inject(function () {
        rootScope.items = [
            {title: 'Item 1 TRUE YES ', verifiedIf: true},
            {title: 'Item 2 TRUE No ', verifiedIf: false}
        ];
        //$rootScope.$apply('')
        el = compile(html.base)(rootScope);

        var scope = el.scope();
        expect(scope.items.length).toEqual(2);

        expect(scope.items[0].title).toEqual('Item 1 TRUE YES ');
        expect(scope.items[1].title).toEqual('Item 2 TRUE No ');

    }));


    it('should evaluate the correct answers', inject(function () {
        rootScope.items = [
            {title: 'Item 1 TRUE YES ', verifiedIf: true, checked: true},
            {title: 'Item 2 TRUE No ', verifiedIf: false}
        ];
        //$rootScope.$apply('')
        el = compile(html.base)(rootScope);

        var scope = el.scope();
        expect(scope.$result.pristine.length).toEqual(2);
        scope.evaluateFromSelf();
        expect(scope.$result.valid.length).toEqual(2);
        expect(scope.$result.invalid.length).toEqual(0);
        expect(scope.$result.neutral.length).toEqual(0);
        expect(scope.$result.pristine.length).toEqual(1);

    }));
    it('should evaluate the wrong answers', inject(function () {
        rootScope.items = [
            {title: 'Item 1 TRUE YES ', verifiedIf: true },
            {title: 'Item 2 TRUE No ', verifiedIf: false, checked: true}
        ];
        //$rootScope.$apply('')
        el = compile(html.base)(rootScope);

        var scope = el.scope();
        expect(scope.$result.pristine.length).toEqual(2);
        scope.evaluateFromSelf();
        expect(scope.$result.valid.length).toEqual(0);
        expect(scope.$result.invalid.length).toEqual(2);
        expect(scope.$result.neutral.length).toEqual(0);
        expect(scope.$result.pristine.length).toEqual(1);

    }));


    it('calculate results', inject(function () {
        rootScope.items = [
            {title: 'Item 1 TRUE YES ', verifiedIf: true, checked: true},
            {title: 'Item 2 TRUE No ', verifiedIf: false, checked: true},
        ];
        //$rootScope.$apply('')
        el = compile(html.base)(rootScope);

        var scope = el.scope();
        scope.evaluateFromSelf();
        scope._calculateStats();

        expect(scope.$resultStats.num_valid).toEqual(1);
        expect(scope.$resultStats.num_invalid).toEqual(1);
        expect(scope.$resultStats.num_neutral).toEqual(0);
        expect(scope.$resultStats.num_pristine).toEqual(0);
    }));


    it('should update and reset', inject(function () {
        rootScope.items = [
            {title: 'Item 1 TRUE YES ', verifiedIf: true, checked: true },
            {title: 'Item 2 TRUE No ', verifiedIf: false, checked: true },
        ];

        el = compile(html.base)(rootScope);
        rootScope.$apply();

        var scope = el.scope();
        var cbs = (el.find('.checkbox'));

        expect(cbs.length).toEqual(2);

        var input = el[0].querySelector('input');
        var submit = el[0].querySelector('button[name="submit"]');
        var reset = el[0].querySelector('button[name="reset"]');


        describe("During result submission", function () {
            if (jQuery) {

                it('should send default values before selection', inject(function () {

                    $(submit).click();
                    expect(scope.$resultStats.num_pristine).toEqual(0);
                    expect(scope.$resultStats.num_valid).toEqual(1);
                    expect(scope.$resultStats.num_invalid).toEqual(1);
                    expect(scope.$resultStats.num_neutral).toEqual(0);
                }));

                it('reset should be allowed', inject(function () {
                    if(scope.items.length > 0){
                        $(reset).click();
                        $(submit).click();

                        expect(scope.$resultStats.num_pristine).toEqual(0);
                        expect(scope.$resultStats.num_valid).toEqual(1);
                        expect(scope.$resultStats.num_invalid).toEqual(1);
                        expect(scope.$resultStats.num_neutral).toEqual(0);
                    }
                }));

                it('should handle checked values', inject(function () {

                    scope.items[0].checked = true;
                    scope.$apply();

                    $(submit).click();
                    expect(scope.$resultStats.num_pristine).toEqual(0);
                    expect(scope.$resultStats.num_valid).toEqual(2);
                    expect(scope.$resultStats.num_invalid).toEqual(0);
                    expect(scope.$resultStats.num_neutral).toEqual(0);
                }));
            }
        });

    }));


});