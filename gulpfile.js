//Modified from https://github.com/driftyco/mobilize/blob/master/gulpfile.js

var GithubApi = require('github');
var gulp = require('gulp');
var path = require('canonical-path');
var pkg = require('./package.json');
var request = require('request');
var q = require('q');
var semver = require('semver');
var through = require('through');

var argv = require('minimist')(process.argv.slice(2));

var _ = require('lodash');
var buildConfig = require('./config/build.config.js');
var changelog = require('conventional-changelog');
var es = require('event-stream');
var irc = require('ircb');
var marked = require('marked');
var mkdirp = require('mkdirp');
var twitter = require('node-twitter-api');

var cp = require('child_process');
var fs = require('fs');

var concat = require('gulp-concat');
var footer = require('gulp-footer');
var gulpif = require('gulp-if');
var header = require('gulp-header');
var jshint = require('gulp-jshint');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var stripDebug = require('gulp-strip-debug');
var template = require('gulp-template');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');
var git = require('gulp-git');
var banner = _.template(buildConfig.banner, { pkg: pkg });
var chalk = require('chalk');

var IS_WATCH = false;

var IS_RELEASE_BUILD = !!argv.r;


if (IS_RELEASE_BUILD) {
    gutil.log(
        gutil.colors.red('--release:'),
        'Building release version (minified, debugs stripped)...'
    );
}


/**
 * Load Test Tasks
 */
require('./config/gulp-tasks/test')(gulp, argv);

/**
 * Load Docs Tasks
 */
require('./config/gulp-tasks/docs')(gulp, argv);
if (argv.dist) {
    buildConfig.dist = argv.dist;
}


gulp.task('default', ['build']);
gulp.task('build', ['bundle', 'sass','html']);
gulp.task('make', ['bundle', 'sass','version','html']);
gulp.task('validate', ['jshint', 'ddescribe-iit', 'karma']);
gulp.task('project',['project','update']);
gulp.task('external', ['vendor']);
gulp.task('test',['karma']);


gulp.task('project',function(){
    var argv = require('minimist')(process.argv.slice(2));

    var projectname = argv.n;
    var folder = argv.n || buildConfig.project.src;
    var projectPath = folder + projectname+"/vendor";


    console.log(chalk.green('Setting up a new project "'+projectname+'" ('+projectPath+')')  );

    gulp.src(buildConfig.dest)
        .pipe(gulp.dest(projectPath));

    console.log(chalk.green('Project has been set up with the distribution files from the framework'));
});


gulp.task('watch', ['build'], function() {
    IS_WATCH = true;
    gulp.watch('js/**/*.js', ['bundle']);
    gulp.watch('scss/**/*.scss', ['sass']);
});



gulp.task('sass', function(done) {
    console.log(chalk.green('Compiling SASS files!')  );
    gulp.src('scss/mobilize.scss')
        .pipe(header(banner))
        .pipe(sass({
            onError: function(err) {

                //If we're watching, don't exit on error
                if (IS_WATCH) {
                    console.log(gutil.colors.red(err));
                } else {
                    done(err);
                }
            }
        }))
        .pipe(concat('mobilize.css'))
        .pipe(gulp.dest(buildConfig.dist + '/css'))
        .pipe(gulpif(IS_RELEASE_BUILD, minifyCss()))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest(buildConfig.dist + '/css'))
        .on('end', done);

    console.log(chalk.green('Compiling SASS files completed!')  );
});



gulp.task('bundle', [
    'scripts',
    'scripts-ng',
    'vendor',
    'version',
], function() {
    gulp.src(buildConfig.getMobilizeBundleFiles().map(function(src) {
        return src.replace(/.js$/, '.min.js');
    }), {
        base: buildConfig.dist,
        cwd: buildConfig.dist
    })
        .pipe(header(buildConfig.bundleBanner))
        .pipe(concat('mobilize.bundle.min.js'))
        .pipe(gulp.dest(buildConfig.dist + '/js'));



    return gulp.src(buildConfig.getMobilizeBundleFiles(), {
        base: buildConfig.dist,
        cwd: buildConfig.dist
    })
        .pipe(header(buildConfig.bundleBanner))
        .pipe(concat('mobilize.bundle.js'))
        .pipe(gulp.dest(buildConfig.dist + '/js'));
});



gulp.task('scripts', function() {
    return gulp.src(buildConfig.getMobilizeFiles())
        .pipe(gulpif(IS_RELEASE_BUILD, stripDebug()))
        .pipe(template({ pkg: pkg }))
        .pipe(concat('mobilize.js'))
        .pipe(header(buildConfig.closureStart))
        .pipe(footer(buildConfig.closureEnd))
        .pipe(header(banner))
        .pipe(gulp.dest(buildConfig.dist + '/js'))
        .pipe(gulpif(IS_RELEASE_BUILD, uglify()))
        .pipe(rename({ extname: '.min.js' }))
        .pipe(gulp.dest(buildConfig.dist + '/js'));
});

gulp.task('scripts-ng', function() {
    return gulp.src(buildConfig.getAngularMobilizeFiles())
        .pipe(gulpif(IS_RELEASE_BUILD, stripDebug()))
        .pipe(concat('mobilize-angular.js'))
        .pipe(header(buildConfig.closureStart))
        .pipe(footer(buildConfig.closureEnd))
        .pipe(header(banner))
        .pipe(gulp.dest(buildConfig.dist + '/js'))
        .pipe(gulpif(IS_RELEASE_BUILD, uglify()))
        .pipe(rename({ extname: '.min.js' }))
        .pipe(gulp.dest(buildConfig.dist + '/js'));
});



gulp.task('version', function() {
    var currentdate  = new Date();

    var date = currentdate.getDate() + "/"+(currentdate.getMonth()+1)+ "/" + currentdate.getFullYear() ;

    var time = pad( currentdate.getHours() ) +
        ':' + pad( currentdate.getMinutes() ) +
        ':' + pad( currentdate.getSeconds() );

    return gulp.src('config/version.template.json')
        .pipe(template({
            pkg: pkg,
            date: date,
            time: time
        }))
        .pipe(rename('version.json'))
        .pipe(gulp.dest(buildConfig.dist));
});


/**
 * Generate vendor file as one big file to reduce get requests
 **/
gulp.task('vendor', function() {
    return gulp.src(buildConfig.vendor.src)
        .pipe(gulpif(IS_RELEASE_BUILD, stripDebug()))
        .pipe(template({ pkg: pkg }))
        .pipe(concat('mobilize.vendor.js'))
        .pipe(header(buildConfig.vendorClosureStart))
        .pipe(footer(buildConfig.vendorClosureEnd))
        .pipe(header(banner))
        .pipe(gulp.dest(buildConfig.dist + '/js'))
        .pipe(gulpif(IS_RELEASE_BUILD, uglify()))
        .pipe(rename({ extname: '.min.js' }))
        .pipe(gulp.dest(buildConfig.dist + '/js'));
});

gulp.task('html', function() {
    return gulp.src(buildConfig.html.src)
        .pipe(gulp.dest(buildConfig.html.dest));
});






//Testing

gulp.task('ddescribe-iit', function() {
    return gulp.src(['test/**/*.js', 'js/**/*.js'])
        .pipe(notContains([
            'ddescribe', 'iit', 'xit', 'xdescribe'
        ]));
});








//Other options :: :: DEV ONLY
gulp.task('gitlogs',function(){
    git.revParse({args:'--short HEAD'}, function (err, hash) {
        //if (err) ...
        console.log('current git hash: '+hash);
    });
});


gulp.task('changelog', function() {
    var dest = argv.dest || 'CHANGELOG.md';
    var toHtml = !!argv.html;
    return makeChangelog(argv).then(function(log) {
        if (toHtml) {
            log = marked(log, {
                gfm: true
            });
        }
        fs.writeFileSync(dest, log);
    });
});


function makeChangelog(options) {
    var codename = pkg.codename;
    var file = options.standalone ? '' : __dirname + '/CHANGELOG.md';
    var subtitle = options.subtitle || '"' + codename + '"';
    var from = options.from;
    var version = options.version || pkg.version;
    var deferred = q.defer();

    git.revParse({args:'--short HEAD'}, function (err, hash) {
        //if (err) ...
        console.log('current git hash: '+hash);
    });

    changelog({
        repository: 'https://savveproducts@bitbucket.org/mobilize.git',
        version: version,
        subtitle: subtitle,
        file: file,
        from: from
    }, function(err, log) {
        if (err) deferred.reject(err);
        else deferred.resolve(log);
    });
    return deferred.promise;
}


gulp.task('jshint', function() {
    return gulp.src(['js/**/*.js'])
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter(require('jshint-summary')({
            fileColCol: ',bold',
            positionCol: ',bold',
            codeCol: 'green,bold',
            reasonCol: 'cyan'
        })))
        .pipe(jshint.reporter('fail'));
});


function notContains(disallowed) {
    disallowed = disallowed || [];

    return through(function(file) {
        var error;
        var contents = file.contents.toString();
        disallowed.forEach(function(str) {
            var idx = disallowedIndex(contents, str);
            if (idx !== -1) {
                error = error || file.path + ' contains ' + str + ' on line ' +
                    contents.substring(0, idx, str).split('\n').length + '!';
            }
        });
        if (error) {
            throw new Error(error);
        } else {
            this.emit('data', file);
        }
    });

    function disallowedIndex(content, disallowedString) {
        var notFunctionName = '[^A-Za-z0-9$_]';
        var regex = new RegExp('(^|' + notFunctionName + ')(' + disallowedString + ')' + notFunctionName + '*\\(', 'gm');
        var match = regex.exec(content);
        // Return the match accounting for the first submatch length.
        return match !== null ? match.index + match[1].length : -1;
    }
}
function pad(n) {
    if (n<10) { return '0' + n; }
    return n;
}
function qRequest(opts) {
    var deferred = q.defer();
    request(opts, function(err, res, body) {
        if (err) deferred.reject(err);
        else deferred.resolve(res);
    });
    return deferred.promise;
}