var pkg = require('../package.json');
var fs = require('fs');

/*
 var DISCOURSE_FILE = __dirname + '/DISCOURSE_POST_URL';
 releasePostUrl: fs.readFileSync(DISCOURSE_FILE).toString(),
 releasePostFile: DISCOURSE_FILE,
 */

module.exports = {
    dist: 'dist', //This is folder all files will be compiled into
    protractorPort: 8876,
    banner: '/*!\n' +
        ' * Copyright 2014 Savv-e.\n' +
        ' * http://www.savv-e.com.au/\n' +
        ' *\n' +
        ' */\n\n',

    bundleBanner: '/*!\n' +
        ' * mobilize.bundle.js is a concatenation of:\n' +
        ' * mobilize.js, angular.js, angular-animate.js,\n' +
        ' * angular-sanitize.js, angular-ui-router.js,\n' +
        ' * and a whole lot of external vendor files to reduce the number of requests and speed up the rendering' +
        ' */\n\n',
    closureStart: '(function() {\n',
    closureEnd: '\n})();',

    /**
     * When the gulp bundle command is run the files in this list are
     * combined to form one js file.
     */
    mobilizeBundleFiles: [

        //All core dependencies
        "config/lib/js/angular.js",
        "config/lib/js/angular-sanitize.js",
        "config/lib/js/angular-ui-router.js",
        "config/lib/js/angular-animate.js",
        "config/lib/js/ui-bootstrap.js",
        "config/lib/js/ui-bootstrap-tpls.js"

        //Add all mobilize files here
    ],
    mobilizeFiles: [
        //The mobilize files first
        'js/mobilize.js',

        //All util files
        'js/utils/*.js',
        'js/utils/**/*.js',

        //All view files
        'js/views/checkboxView.js',

        //Add all angular mobilizefiles here
    ],
    //Find out a way of loading this in order
    angularMobilizeFiles: [
        //All mobilize files
        'js/angular/main.js',
        'js/angular/config/mobilizeConfig.js',
        'js/angular/config/mobilizeTplConstants.js',

        //checkboxes
        'js/angular/service/delegateService.js',
        'js/angular/service/checkboxDelegate.js',
        'js/angular/service/checkbox.js',
        'js/angular/filter/objectKeyFilter.js',
        'js/angular/filter/objectKeyValidFilter.js',
        'js/angular/directive/checkboxView.js',
        'js/angular/controller/checkboxController.js',

    ],
    vendor: {
        src: [
            "config/lib/js/ionic.bundle.js",
            "config/lib/js/angular-translate.js",
            "config/lib/js/angular-translate-loader-static-files.js",
            "config/lib/js/angular-translate-loader-url.js"
        ],
        dest: "dist/vendor"
    },
    html: {
        src: "html/*",
        dest: "dist"
    },
    project: {
        src: "../"
    },

    getMobilizeBundleFiles: function () {
        return this.mobilizeBundleFiles.concat(this.getMobilizeFiles());
    },
    getMobilizeFiles: function () {
        return this.mobilizeFiles.concat(this.getAngularMobilizeFiles());
    },
    getAngularMobilizeFiles: function () {
        return this.angularMobilizeFiles;
    },
    //Exclamation can be no longer than 14 chars
    exclamations: [
        "Aah", "Ah", "Aha", "All right", "Aw", "Ay", "Aye", "Bah", "Boy", "By golly", "Boom", "Cheerio", "Cheers", "Come on", "Crikey", "Dear me", "Egads", "Fiddle-dee-dee", "Gadzooks", "Gangway", "G'day", "Gee whiz", "Gesundheit", "Get outta here", "Gosh", "Gracious", "Great", "Gulp", "Ha", "Ha-ha", "Hah", "Harrumph", "Hey", "Hooray", "Hurray", "Huzzah", "I say", "Look", "Look here", "Long time", "Lordy", "Most certainly", "My my", "My word", "Oh", "Oh-oh", "Oh no", "Okay", "Okey-dokey", "Ooh", "Oye", "Phew", "Quite", "Ready", "Right on", "Roger that", "Rumble", "Say", "See ya", "Snap", "Sup", "Ta-da", "Take that", "Tally ho", "Thanks", "Toodles", "Touche", "Tut-tut", "Very nice", "Very well", "Voila", "Vroom", "Well done", "Well, well", "Whoa", "Whoopee", "Whew", "Word up", "Wow", "Wuzzup", "Ya", "Yea", "Yeah", "Yippee", "Yo", "Yoo-hoo", "You bet", "You don't say", "You know", "Yow", "Yum", "Yummy", "Zap", "Zounds", "Zowie"
    ],

    //Message can be no longer than it is. Currently it's 126 chars with the short git urls,
    //and can have up to a 14 char long exclamation prepended.
    releaseMessage: function () {
        return this.exclamations[Math.floor(Math.random() * this.exclamations.length)] + '! ' +
            'Savv-e has released new version of Mobilize ' + pkg.version + ' "' + pkg.codename + '"! ';
    }
};