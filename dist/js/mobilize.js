/*!
 * Copyright 2014 Savv-e.
 * http://www.savv-e.com.au/
 *
 */

(function() {
//Create a global mobilize objects and its namespace..
//Time to get started
window.mobilize = window.mobilize || {};
window.mobilize.views={};
window.mobilize.utils={};
window.mobilize.version = '1.0.1';
window.mobilize.codename = 'yomoto';
window.mobilize.utils.shuffle = function shuffle(array) {
//    var currentIndex = array.length, temporaryValue, randomIndex ;
//
//    // While there remain elements to shuffle...
//    while (0 !== currentIndex) {
//
//        // Pick a remaining element...
//        randomIndex = Math.floor(Math.random() * currentIndex);
//        currentIndex -= 1;
//
//        // And swap it with the current element.
//        temporaryValue = array[currentIndex];
//        array[currentIndex] = array[randomIndex];
//        array[randomIndex] = temporaryValue;
//    }
//
//    return array;
}

(function(mobilize,ionic){
    'use strict',
    mobilize.views.Checkbox = ionic.views.View.inherit({
        initialize : function(options){
            options = ionic.extend({

            },options);
        },
        show:function(){
            //handle show functionalities
        },
        hide: function(){
            //Handle hide functionalites
        }

     });

}(window.mobilize,window.ionic));
window.MobilizeModule = angular.module('mobilize', ['ionic','ngAnimate', 'ngSanitize', 'ui.router']),
    extend = angular.extend,
    forEach = angular.forEach,
    isDefined = angular.isDefined,
    isNumber = angular.isNumber,
    isString = angular.isString,
    jqLite = angular.element;




(function (MobilizeModule, mobilize, angular) {
    'use strict';

    MobilizeModule.constant('$mobilizeTPL',{
        checkboxDefaultTpl : '' +
            '<div class="checkbox-wrapper">' +
                '<div class="checkbox-header">' +
                    '<h3 class="checkbox-title" ng-bind-html="title" ng-if="title"></h3>'+
                    '<h4 class="checkbox-title" ng-bind-html="subTitle" ng-if="subTitle"></h4>'+

                    '<p class="checkbox-header-content" ng-bind-html="headerContent" ng-if="headerContent"></p>'+
                '</div>'+
                '<ul class="checkbox-container">'+
                    '<li class="item item-checkbox">'+
                        '<ion-checkbox ng-repeat="item in items" '+
                        'ng-model="item.checked" '+
                        'ng-checked="item.checked || false" '+
                        'ng-class="item.ngClass" '+
                        'ng-disabled="$resultStats.evaluated" '+
                        '> '+
                            '<span class="item-title" translate>{{ item.title }}</span> '+
                        '</ion-checkbox> '+
                    '</li> '+
                '</ul> '+

                '<div class="checkbox-footer" ng-show="buttons.length"> '+
                    '<button ng-click="{{button.onTap}}(items,button,$event)" '+
                    'ng-repeat="button in buttons" '+
                    'class="button" ' +
                    'name="{{button.name}}"   '+
                    'ng-class="button.type || button-default" '+
                    'ng-bind-html="button.text" '+
                    'ng-show="{{button.showIf || true}}" '+
                    '> '+
                    '</button> '+
                '</div> '+
            '</div>'
    });

})(window.MobilizeModule,window.mobilize,window.angular);
(function (mobilize) {
    mobilize.delegateService = function(methodNames) {

        if (methodNames.indexOf('$getByHandle') > -1) {
            throw new Error("Method '$getByHandle' is implicitly added to each delegate service. Do not list it as a method.");
        }

        function trueFn() { return true; }

        return ['$log', function($log) {

            /*
             * Creates a new object that will have all the methodNames given,
             * and call them on the given the controller instance matching given
             * handle.
             * The reason we don't just let $getByHandle return the controller instance
             * itself is that the controller instance might not exist yet.
             *
             * We want people to be able to do
             * `var instance = $ionicScrollDelegate.$getByHandle('foo')` on controller
             * instantiation, but on controller instantiation a child directive
             * may not have been compiled yet!
             *
             * So this is our way of solving this problem: we create an object
             * that will only try to fetch the controller with given handle
             * once the methods are actually called.
             */
            function DelegateInstance(instances, handle) {
                this._instances = instances;
                this.handle = handle;
            }
            methodNames.forEach(function(methodName) {
                DelegateInstance.prototype[methodName] = instanceMethodCaller(methodName);
            });


            /**
             * The delegate service (eg $ionicNavBarDelegate) is just an instance
             * with a non-defined handle, a couple extra methods for registering
             * and narrowing down to a specific handle.
             */
            function DelegateService() {
                this._instances = [];
            }
            DelegateService.prototype = DelegateInstance.prototype;
            DelegateService.prototype._registerInstance = function(instance, handle, filterFn) {
                var instances = this._instances;
                instance.$$delegateHandle = handle;
                instance.$$filterFn = filterFn || trueFn;
                instances.push(instance);

                return function deregister() {
                    var index = instances.indexOf(instance);
                    if (index !== -1) {
                        instances.splice(index, 1);
                    }
                };
            };
            DelegateService.prototype.$getByHandle = function(handle) {
                return new DelegateInstance(this._instances, handle);
            };

            return new DelegateService();

            function instanceMethodCaller(methodName) {
                return function caller() {
                    var handle = this.handle;
                    var args = arguments;
                    var foundInstancesCount = 0;
                    var returnValue;

                    this._instances.forEach(function(instance) {
                        if ((!handle || handle == instance.$$delegateHandle) && instance.$$filterFn(instance)) {
                            foundInstancesCount++;
                            var ret = instance[methodName].apply(instance, args);
                            //Only return the value from the first call
                            if (foundInstancesCount === 1) {
                                returnValue = ret;
                            }
                        }
                    });

                    if (!foundInstancesCount && handle) {
                        return $log.warn(
                            'Delegate for handle "' + handle + '" could not find a ' +
                                'corresponding element with delegate-handle="' + handle + '"! ' +
                                methodName + '() was not called!\n' +
                                'Possible cause: If you are calling ' + methodName + '() immediately, and ' +
                                'your element with delegate-handle="' + handle + '" is a child of your ' +
                                'controller, then your element may not be compiled yet. Put a $timeout ' +
                                'around your call to ' + methodName + '() and try again.'
                        );
                    }
                    return returnValue;
                };
            }

        }];
    }
}(window.mobilize));

(function (MobilizeModule,mobilize) {
    /**
     * @ngdoc service
     * @name $mobilizeCheckboxDelegate
     * @module mobilize
     *
     * @description
     * Delegate for controlling the {@link mobilize.directive:checkboxView} directive
     *
     * Methods called directly on the $mobilizeCheckboxDelegate service will control all $mobilizeCheckbox
     * directives. Use the {@link mobilize.service:$mobilizeCheckboxDelegate#$getByHandle $getByHandle}
     * to control specific instances
     *
     * @usage
     * ```
     *
     * ```
     */
    MobilizeModule.service('$mobilizeCheckboxDelegate',mobilize.delegateService([
        /**
         * @ngdoc method
         * @name $ionicTabsDelegate#$getByHandle
         * @param {string} handle
         * @returns `delegateInstance` A delegate instance that controls only the
         * {@link ionic.directive:ionTabs} directives with `delegate-handle` matching
         * the given handle.
         *
         * Example: `$ionicTabsDelegate.$getByHandle('my-handle').select(0);`
         */
    ]));
}(window.MobilizeModule,window.mobilize));

(function (MobilizeModule) {

    /**
     * @author sundar
     * @name $mobilizeCheckbox
     * @module mobilize
     * @since 0.4
     * @copyright  Copyright (c) 2014 Savv-e Pty Ltd
     * Related: {@link mobilize.controller:mobilizeCheckbox mobilizeCheckbox controller}.
     *
     *
     * The Checkbox is collection of checkbox pre-configured with all common functionalities in eLearning.
     *
     * **Notes:**
     *  -Events being broadCasted :
     *
     *
     *  @usage:
     *
     *
     */


    MobilizeModule
        .factory('$mobilizeCheckbox', [
            '$rootScope',
            '$ionicBody',
            '$compile',
            '$timeout',
            '$q',
            '$log',
            function ($scope, $ionicBody, $compile, $timeout, $q, $log) {
                var config = {};
                var checkboxStack = [];
                var $mobilizeCheckbox = {
                    /**
                     * @ngdoc method
                     * @description
                     * Show a list of checkboxes . The master controller will manage the collection of checkboxes
                     *
                     * @name $mobilizeCheckbox#show
                     * @param {object} options : The options for the new checkbox manager of the form
                     *
                     * ```
                     * {
                     *  title: '', //The title of the checkboxes
                     *  template : '', //String(optional) The html template to create the checkboxes
                     *  templateUrl: '', //String(optional) The URL of an html template to place the checkboxes in
                     *  scope : null, //Scope(optional) The scope to link to the popup content.
                     *  buttons :[
                     *      {
                     *          text:'Clear All',
                     *          type:'button-default',
                     *          onTap:function(e){
                     *              //e.preventDefault() will stop the checkbox from being cleared on tap
                     *          }
                     *      },
                     *      {
                     *          text:'Submit',
                     *          type:'button-default',
                     *          onTap:function(e){
                     *              //e.preventDefault() will stop the checkbox from being evaluated
                     *              return scope.data.response;
                     *          }
                     *      },
                     *  ]
                     * }
                     * ```
                     * @returns {object} A promise which is resolved when the checkboxes are evaluated.
                     */

                    show:function(){}, //showCheckbox,

                    /**
                     * @ngdoc method
                     * @name @mobilizeCheckbox#alert
                     * @description Show a alert popup with a message  that the user can tap to close
                     * ```
                     * {
                     *      title:'',//Title of the alert message
                     *      subTitle:'',//Sub-title of the alert message
                     *      template:'',//String(optional) The html template to place in the popup body
                     *      templateUrl:'',//String(optional) The URL of an html template to place in the popup body,
                     *      buttons :[
                     *      {
                     *          text:'Ok',
                     *          type:'button-default',
                     *          onTap:function(e){
                     *              //Any additional functionalities you need on the tap
                     *          }
                     *      },
                     *      {
                     *          text:'Submit',
                     *          type:'button-default',
                     *          onTap:function(e){
                     *              //Any additional functionalities you need on the tap
                     *          }
                     *      },
                     *      ]
                     * }
                     * ```
                     */
                    alert: function(){}, //showAlert,

                    /**
                     * @ngdoc method
                     * @name $mobilizeCheckbox#confirm
                     * @description
                     * Show a simple confirm popup with a Cancel and OK button.
                     *
                     * Resolves the promise with true if the user presses the OK button, and false if the
                     * user presses the Cancel button.
                     *
                     * @param {object} options The options for showing the confirm popup, of the form:
                     *
                     * ```
                     * {
                     *   title: '', // String. The title of the popup.
                     *   subTitle: '', // String (optional). The sub-title of the popup.
                     *   template: '', // String (optional). The html template to place in the popup body.
                     *   templateUrl: '', // String (optional). The URL of an html template to place in the popup   body.
                     *   cancelText: '', // String (default: 'Cancel'). The text of the Cancel button.
                     *   cancelType: '', // String (default: 'button-default'). The type of the Cancel button.
                     *   buttons :[
                     *      {
                     *          text:'Ok',
                     *          type:'button-default',
                     *          onTap:function(e){
                     *              //Any additional functionalities you need on the tap
                     *          }
                     *      },
                     *      {
                     *          text:'Submit',
                     *          type:'button-default',
                     *          onTap:function(e){
                     *              //Any additional functionalities you need on the tap
                     *          }
                     *      },
                     *      ]
                     * }
                     * ```
                     *
                     * @returns {object} A promise which is resolved when the popup is closed. Has one additional
                     * function `close`, which can be called with any value to programmatically close the popup
                     * with the given value.
                     */
                    confirm:function(){}, //showConfirm,

                    /**
                     * @private For testing
                     */
                    _createCheckbox: function(){} //createCheckbox
                };


                return $mobilizeCheckbox;


                var showCheckbox=function(){}
                var showConfirm=function(){}
                var showAlert=function(){}
                var createCheckbox=function(){}
                /**
                 * @ngdoc Controller
                 * @name mobilizeCheckbox
                 * @module mobilize
                 * @description
                 * Instantiated by the {@link mobilize.service::$mobilizeModal} service
                 *
                 * Be sure to call remove when done with the check box to clean up and avoid memory leaks
                 *
                 * Events being broadcasted ::
                 *
                 */
                var CheckboxView = mobilize.views.Checkbox.inherit({
                    /**
                     * @ngdoc method
                     * @name mobilizeCheckbox#initialize
                     * @description Creats a checkbox controller instance
                     * @param options
                     */
                    initialize: function (options) {
                        mobilize.views.Checkbox.prototype.initialize.call(this, options);
                        this.animation = options.animation || 'fade-in';
                    },

                    show: function (target) {
                        var self = this;
                        if (self.scope.$$destroyed) {
                            $log.error('Cannot call ' + self.viewType + '.show() after remove(). Please create a new ' + self.viewType + ' instance.');
                            return;
                        }

                        var checkboxEl = jqLite(self.modalEl);

                        //Handle other show functionalities here
                    },

                    hide: function () {
                        //Handle hide functionalities
                    },
                    remove: function () {
                        //handle remove functionalities
                    }
                });


            }]);

}(window.MobilizeModule));

(function (MobilizeModule,mobilize) {
    /**
     * @ngdoc filter
     * @name objectkey
     * @module mobilize
     * @delegate
     *
     *
     * @description
     * Filter an array of objects by key
     * Works very similiar to the native angular JS Filtering ( friend in friends | filter: { name: query, age: '20' })
     *
     * Basic usage:
     * ```
     *  <p> {{"items | objectkey:'checked':true" }}</p>
     * ```
     * Filter can also be accessed through the $filter service like so
     * ```
     * $filter('objectkey')($scope.items,'checked',true)
     * ```
     * @param {array=} An array of objects fed in as items
     * @param {string=} The key of each object within the array to compare each element to
     * @param {bool | string =} the value to which to compare the string to
     */
    MobilizeModule.filter(
        'objectkey',
        function(){
            return function (items, key,value) {
                var filtered = [];
                forEach(items,function(it){
                    if(it[key]==value){
                        filtered.push(it);
                    }
                });
                return filtered;
            };
        }
    );
}(window.MobilizeModule,window.mobilize));

(function (MobilizeModule, mobilize) {
    /**
     * @ngdoc filter
     * @name objectkey
     * @module mobilize
     * @delegate
     *
     *
     * @description
     * Loops through an array of objects and returns true if atleast one of the object in the array has a valid key
     * More useful if you are using this with ng-Show , ng-If, ng-hide and the like
     *
     *
     * Basic usage:
     * ```
     *  <p> {{"items | objectkey:'checked':true" }}</p>
     * ```
     * Filters can also be accessed through the $filter service like so
     * ```
     * $filter('objectkeyValid')($scope.items,'checked',true)
     * ```
     * @param {array=} An array of objects fed in as items
     * @param {string=} The key of each object within the array to compare each element to
     * @param {bool | string =} the value to which to compare the string to
     */
    MobilizeModule.filter(
        'objectkeyValid',
        function () {
            return function (items, key, value) {
                if (angular.isArray(items)) {

                    var valid = false;
                    forEach(items, function (it) {
                        if(!valid){
                            valid = (isDefined(it[key]) && (it[key] == value));
                        }
                    });

                    return valid;
                }
            };
        }
    );
}(window.MobilizeModule, window.mobilize));

/**
 * @ngdoc directive
 * @name mobilizeCheckbox
 * @module mobilize
 * @delegate mobilize.service.checkboxDelegate
 * @restrict 'AEC'
 * @controller mobilize.controller.checkboxController
 * @description
 * Creates a checkbox manager for controlling a collection of checkbox
 *
 * Events::
 *  The following events will be emited by the checkbox to allow for handling
 *
 *  $broadcast -- dispatches the event downwards to all child scopes,
 *      --NONE--
 *  $emit -- dispatches the event upwards through the scope hierarchy.
 *      $mobilizeCheckbox.evaluated : Emitted when the mobilize Checkbox is evaluated .
 *           This has two parameters
 *              @param {array=}    $resultStats : Provides a processed result list statistics
 *              @param {array=}    $result      : Exposes the items as objects .
 *     $mobilizeCheckbox.destroyed   : Emmitted when the mobilize Checkbox is destroyed
 *     $mobilizeCheckbox.reset       : Emmitted when the mobilize Checkbox is destroyed
 *
 *
 * @usage
 * ```
         <mobilize-checkbox
         mobi-items="{{checkboxItems}}"
         mobi-randomize="{{checkboxRandomize || false}}"
         mobi-title="{{checkboxTitle || 'blah blah blah TITLE COMES HERE '}}"
         mobi-sub-title="{{checkboxSubTitle || 'blah blah blah SUB TITLE COMES HERE '}}"
         mobi-header-content="{{checkboxHeaderContent || 'blah blah blah'}}"
         mobi-tpl="partial/partial-checkbox.html"
         mobi-answer-highlights="false"
         delegate-handle="cb-1">

         <div class="addl-content">
         <span class='caf-content' ng-if="$resultStats.num_invalid == 0 && $resultStats.evaluated">
         SOME CAF CONTENT COMES HERE
         </span>
         <span class='waf-content' ng-if="$resultStats.num_invalid > 0 && $resultStats.evaluated">
         SOME WAF CONTENT COMES HERE
         </span>
         </div>
         </mobilize-checkbox>
 *```
 * Js:
 * ```
         mymodule.controller('AppCtrl', ['$scope', function ($scope, $mobilizeCheckbox) {
                $scope.checkboxItems = [
                    {title: 'Item 1 TRUE YES ', verifiedIf: true},
                    {title: 'Item 2 TRUE No ', verifiedIf: false},
                    {title: 'Item 3 TRUE No ', verifiedIf: false},
                    {title: 'Item 4 TRUE No ', verifiedIf: false},
                    {title: 'Item 5 TRUE YES ', verifiedIf: true},
                    {title: 'Item 6 TRUE YES ', verifiedIf: true},
                ];
                $scope.checkboxHeaderContent = "<p>SOME HEADER CONTENT COMES HERE </p> <b>HEADER CONTENT CAN HAVE TITLES TOO</b> <h4>AND NICE FORMATTING USING HTML TAGS</h4>";
            }])
 * ```
 *
 *
 * Note :
 * To keep it clean and consise and also to make sure the directive does not break during compilation
 * please add any additional content into the <div class='addl-content'></div>
 * Content inside the div will be compiled into the directive. So variables can be used in inside as shown in the example
 *
 *  @param {string=}            mobi-items A json string of items .
 *  @param {boolean=}           mobi-randomize Randomize the checkbox on load
 *  @param {string=}            mobi-title Title field on the partial
 *  @param {string=}            mobi-sub-title sub title field on the partial
 *  @param {string=}            mobi-header-content  Header content on the partial
 *  @param {string=} delegate-handle The handle used to identify this checkboxView with {@link mobilize.service::$mobilizeCheckboxDelegate}
 *  @param {optional string=}   mobi-tpl  If a template is not provided the default structure will be loaded. This is a useful option to customize the checkbox to requirements
 *  @param {optional string=}   mobi-prevent-default-buttons This will prevent angular from loading the default button if you choose to replace the buttons in the addl-content
 *
 */
(function (MobilizeModule, mobilize, angular) {

    function toBoolean(value) {
        if (value && value.length !== 0) {
            var v = ("" + value).toLowerCase();
            value = !(v == 'f' || v == '0' || v == 'false' || v == 'no' || v == 'n' || v == '[]');
        } else {
            value = false;
        }
        return value;
    }




    MobilizeModule
        .directive('mobilizeCheckbox', [
            '$controller',
            '$timeout',
            '$ionicBind',
            '$http',
            '$templateCache',
            '$compile',
            '$mobilizeTPL',
            function ($controller, $timeout, $ionicBind, $http, $templateCache, $compile,$mobilizeTPL) {
                return {
                    restrict: 'AEC',
                    transclude: true,
                    scope: true,
                    controller: function () {
                    },
                    compile: function (element, attr, transclude) {
                        var innerElement;
                        //set up the manager
                        element.addClass('checkbox-manager');
                        return {pre: preLink};


                        /**
                         * Prelink functionality on compile
                         * @param $scope
                         * @param $element
                         * @param $attr
                         */
                        function preLink($scope, $element, $attr) {
                            var checkboxCtrl, checkboxView;
                            var buttons = [];

                            $ionicBind($scope, $attr, {
                                mobiTitle: '@',
                                mobiSubTitle: '@',
                                mobiHeaderContent: '@'
                            });


                            //Make sure the items get updated every time there is an update
                            $attr.$observe('mobiItems', function (items) {
                                checkboxCtrl.setItems(items);
                            });
                            $attr.$observe('mobiRandomize', function (randomize) {
                                checkboxCtrl.setRandomize(randomize);
                            });
                            $attr.$observe('mobiTitle', function (title) {
                                checkboxCtrl.setTitle(title);
                            })
                            $attr.$observe('mobiSubTitle', function (title) {
                                checkboxCtrl.setSubTitle(title);
                            })
                            $attr.$observe('mobiHeaderContent', function (title) {
                                checkboxCtrl.setHeaderContent(title);
                            })
                            $attr.$observe('mobiType', function (type) {
                                checkboxCtrl.setType(type);
                            })
							
                            transclude($scope, function (clone) {
                                children = clone;
                            });


                            //Load the html from a tpl or from the default template
                            if (isDefined($attr.mobiTpl)) {
                                //A template has been defined. Load  the template from there
                                $http.get($attr.mobiTpl, {cache: $templateCache})
                                    .success(function (html) {
                                        innerElement = jqLite(html);
                                        innerElement.append(element.contents());

                                        element
                                            .append($compile(innerElement.html())($scope))
                                            .append(children);

                                    });

                            }
                            else {
                                //Load it from the default
                                innerElement = jqLite($mobilizeTPL.checkboxDefaultTpl);
                                innerElement.append(element.contents());

                                element
                                    .append($compile(innerElement.html())($scope))
                                    .append(children);
                            }


                            if ((isDefined($attr.mobiPreventDefaultButtons) && !$attr.mobiPreventDefaultButtons) || !isDefined($attr.mobiPreventDefaultButtons)) {
                                buttons = [
                                    {
                                        name: 'reset',
                                        text: 'Reset',
                                        type: 'button-default',
                                        onTap: 'reset',
                                        showIf: true
                                    },
                                    {
                                        name: 'submit',
                                        text: 'Submit',
                                        type: 'button-default',
                                        onTap: 'evaluate',
                                        disabledIf: "!(items | objectkeyValid:'checked':true) || $resultStats.evaluated"
                                    }
                                ]
                            }
                            //Set up watches on the variables here
                            var checkboxOptions = {
                                el: $element[0],
                                delegateHandle: $attr.delegateHandle,
                                items: $attr.mobiItems,
                                title: $attr.mobiTitle,
                                subTitle: $attr.mobiSubTitle || null,
                                randomize: toBoolean($attr.mobiRandomize),
                                buttons: buttons
                            };


                            //Set up the controller for checkboxes
                            checkboxCtrl = $controller('$mobilizeCheckbox', {
                                $scope: $scope,
                                checkboxOptions: checkboxOptions
                            });

                            checkboxCtrl.initialize();


                            //Ability to reset the checkbox items on the controller. (to allow for randomize)
                            $scope._checkboxView = $scope.$parent.checkboxView = checkboxCtrl.checkboxView;

                            //For testing
                            $scope._checkboxCtrl = checkboxCtrl;
                        }
                    }
                };
            }]);
}(window.MobilizeModule, window.mobilize, window.angular));
/**
 * @private
 */
(function (MobilizeModule) {
    var shuffleArray = function (array) {
        var m = array.length, t, i;

        // While there remain elements to shuffle
        while (m) {
            // Pick a remaining element…
            i = Math.floor(Math.random() * m--);

            // And swap it with the current element.
            t = array[m];
            array[m] = array[i];
            array[i] = t;
        }

        return array;
    }

    MobilizeModule.controller('$mobilizeCheckbox', [
        '$scope',
        'checkboxOptions',
        '$mobilizeCheckboxDelegate',
        '$timeout',
        '$window',
        '$location',
        '$document',
        function ($scope, checkboxOptions, $mobilizeCheckboxDelegate, $timeout, $window, $location, $document) {
            //Set up
            var self = this;
            //testing
            self.checkboxOptions = checkboxOptions;

            var scope = self.$scope = $scope;
            var element = checkboxOptions.el;
            var $element = self.$element = jqLite(element);
            var items;
            var $responses = $scope.$responses = [];
			var type = self.type = self.$scope.mobiType = "single";
			
			
            var initialized = false;

            $scope.$result = {
                valid:[],
                invalid : [],
                neutral : [],
                pristine: []
            };

            $scope.$resultStats = {
                num_valid : 0,
                num_invalid : 0,
                num_neutral : 0,
                num_pristine : 0,
                evaluated : false
            };




            //Create the checkbox view
            var checkboxView = self.checkboxView = new mobilize.views.Checkbox(checkboxOptions);

            //Bind the self to the element as a controller so other directives can require this controller
            ($element.parent().length ? $element.parent() : $element).data('$$mobilizeCheckboxController', self);

            //Deregister the instance from the delegate
            var deregisterInstance = $mobilizeCheckboxDelegate.
                _registerInstance(self, checkboxOptions.delegateHandle, function () {
                    return true
                });
				
			
            /**
             * Initialize the check box controller.
             * @param options
             * @returns {boolean}
             */
            self.initialize = function (options) {

                if(self.initialized){
                    return false;
                }


                self.initialized = true;
                var options=angular.extend({},checkboxOptions,(options||{}));
                if (!options) {
                    return false;
                }


                self.setRandomize(options.randomize || false)
                self.setButtons(options.buttons||[]);

                if (isDefined(options.items)) {
                    //items is defined
                    self.setItems(options.items);
                }
                //By default all the current status are unknown
                $scope.$result.pristine=items;
            }

            /**
             * Set the visible buttons to show. This is defined in the directives and passed through
             *```
             *   buttons :[
             *      {
             *          text:'Ok',
             *          type:'button-default',
             *          onTap:functionNameOnTheScope
             *      },
             *      {
             *          text:'Submit',
             *          type:'button-default',
             *          onTap:functionNameOnTheScope
             *      },
             *      ]
             * }
             * ```
             * @param buttons
             * @returns {*}
             */
            self.setButtons= function(buttons){
                buttons=buttons||[];
                $scope.buttons=buttons;
                return self;
            }
			/**
             * Set up whether you can select only one checkbox or multiple checkboxes
             * @param objectType "single" or "multiple"
             */
			self.setType = function(objectType){
				type = self.type = self.$scope.mobiType = objectType;
				return self;
			}

            /**
             * Randomize the checkboxes or not
             * @param randomize
             */
            self.setRandomize = function (randomize) {
                self.randomize = randomize || false;
                return self;
            }
            /**
             * Set up the items in the checkbox
             * @param its
             */
            self.setItems = function (its) {
                if (angular.isString(its)) {
                    items = angular.fromJson(its);
                }
                else {
                    items = its;
                }

                self.items = $scope.items = items;

                //Randomize the checkboxes
                if(self.randomize){
                    shuffleArray(self.items);
                }
				
				angular.forEach(self.items, function(item, key) {
					item.checked = item.checked || false;
				});
				
				
                return self;
            }
            /**
             * Set up a title of the check box
             * @param title
             * @returns {*}
             */
            self.setTitle = function (title) {
                $scope.title = title;
                return self;
            }

            /**
             * Set up sub title
             * @param subTitle
             * @returns {*}
             */
            self.setSubTitle = function (subTitle) {
                $scope.subTitle = subTitle;
                return self;
            }

            /**
             * Set content in the header
             * @param headerContent
             * @returns {*}
             */
            self.setHeaderContent = function (headerContent) {
                $scope.headerContent = headerContent;
                return self;
            }

            /**
             * Returns the check box view object
             * @returns {*}
             */
            self.getCheckboxView = function () {
                return self.checkboxView;
            }



            /**
             * Calculate stats for the check box
             */
            self.calculateStats = function(){
                $scope.$resultStats.num_valid = ($scope.$result.valid &&  $scope.$result.valid.length);
                $scope.$resultStats.num_invalid = ($scope.$result.invalid &&  $scope.$result.invalid.length);
                $scope.$resultStats.num_neutral = ($scope.$result.neutral &&  $scope.$result.neutral.length);
                $scope.$resultStats.num_pristine = ($scope.$result.pristine &&  $scope.$result.pristine.length);

                $scope.$emit('$mobilizeCheckbox.evaluated',{'$resultStats':$scope.$resultStats, '$result':$scope.$result})
            }



            /**
             * Watch the result variable to calculate the stats
             */
            $scope.$watch('$result',function(){
                self.calculateStats();
			});
            /**
             * View functionalities
             * @param items
             */
            $scope.evaluate = function(items , reset ){
                reset = reset || false;
                var valid=[]
                    ,invalid=[]
                    ,neutral=[]
                    ,pristine=[];

                $scope.$resultStats.evaluated = true;


                angular.forEach(items, function(item, key) {
                    item.ngClass= [];

                    //Mark the right and wrong answers
                    if(item.verifiedIf){
                        item.ngClass.push('mobi-correct');
                    }
                    else{
                        item.ngClass.push('mobi-incorrect');
                    }


                    //Mark whether the checkbox has been clicked before
                    if(!isDefined(item.checked)){
                        pristine.push(item);
                        item.ngClass.push('mobi-pristine');
                    }

                    //if we do not know whether the item has a verification at all
                    if(!isDefined(item.verifiedIf)){
                        neutral.push(item);
                        item.ngClass.push('mobi-neutral');
                        return true;
                    }
                    if((isDefined(item.checked) && item.checked) && item.verifiedIf){
                        valid.push(item);
                        item.ngClass.push('mobi-valid');
                        return true;
                    }

                    if(!item.verifiedIf && (!isDefined(item.checked) || !item.checked)){
                        valid.push(item);
                        item.ngClass.push('mobi-valid');
                        return true;
                    }

                    item.ngClass.push('mobi-invalid');
                    invalid.push(item);
                    return true;
                });


                $scope.$result = {
                    valid: valid,
                    invalid : invalid,
                    neutral : neutral,
                    pristine: pristine
                };


            }


            /**
             * Evaluate from the values in the root scope
             */
            $scope.evaluateFromSelf = function(){
                return $scope.evaluate($scope.items);
            }

            /**
             * Reset the check boxes
             * @param items
             */
            $scope.reset = function(items){
                angular.forEach(items, function(item, key) {
                    item.checked=false;
                });

                $scope.$result = {
                    valid: [],
                    invalid : [],
                    neutral : [],
                    pristine: $scope.items
                };
                $scope.$resultStats.evaluated = false;
                $scope.$emit('$mobilizeCheckbox.reset');
            }


            $scope.$on('$destroy', function () {
                //Stop the watch
                $scope.$watch('$result',angular.noop);

                self.$scope = null;
                if (self.items) {
                    self.items = null;
                }

                if (self.content) {
                    self.content.element = null;
                    self.content = null;
                }
                $scope.$emit('$mobilizeCheckbox.destroyed');
            });


            //Private functions
            /**
             * Shuffle up the options
             */
            $scope.shuffleOptions = function(){
                shuffleArray(self.items);
            }

            //For testing
            $scope._calculateStats  = self.calculateStats;
        }
    ]);


}(window.MobilizeModule));
})();