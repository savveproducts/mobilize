/**
 * @license Savv-e Australia(www.codefuse.com.au)
 * (c) 2014 Savv-e Australia(www.codefuse.com.au)
 * License: MIT
 */
 
 //File Constants.js
 (function() {

     /*
        This activates strict mode. Strict mode:
        Catches coding bloopers, throws exceptions, prevents errors and stops "unsafe" actions (such as gaining access to the global object),
        disables features that are confusing or poorly thought out.
     */
     'use strict';

    /* Defines the angular application/module (ng-app)*/
     angular.module('ui.modulename',[])
        /* Constant is just data that is accessed regularly throughout the app
           A constant can be injected into a module configuration function
           Define constant as an object + list it's properties */
		.constant('moduleNameConfig',{
			constant1: 'value1',
			constant2: 'value2',
			constant3: 'value3'
		})
		.constant('someOtherConstant',{
			gen1: 'value',
			gen2: 'value'
		})
 })();
 
 //File Service.js
 (function() {
	'use strict';
	angular.module('ui.modulename',[])
        // Factory is a stateless object that contains some useful functions. Unlike a service the value of this object
        // is returned before the data is sent to the controller, filter, directive, etc.
        // Is used to make the app modular, therefore writing the object once to be used several times by controllers,
        // filter, directives, etc. Below $document, $window are dependencies of this factory.
		.factory('$moduleFactoryName',['$document', '$window',function($document,$window){
			return {
				someFactoryVariable : "somefactoryvalue",
				someFactoryFunction : function(scope,attrs){ return "something";} // You can pass in scope , the target element , the event pretty much anything
				
			};
        }])
		.factory('$moduleFactoryName2',['$scope', '$element', '$window',function($scope,$element,$window){
			return {
				someFactoryVariable : "somefactoryvalue",
				someFactoryFunction : function(scope,attrs){ return "something";} // You can pass in scope , the target element , the event pretty much anything

			};
		}]);
			
	
 })();
 
 
 //File Controller.js
(function() {
	'use strict';
	 angular.module('ui.modulename',[])
		.controller('moduleNameController',['$scope', '$element', '$window', '$attrs', 'moduleNameConfig', '$moduleFactoryName', 'awholebunchofvars'
						, function ($scope, $element, $window, $attrs, moduleNameConfig, $moduleFactoryName, awholebunchofvars) { 
							this.scope = $scope;
							
							//Do things with scope
							//do more awesome things with the scope
							//do pretty much anything with the scope.
							//use angular statements to help you through..
							//And then define anything and everything on the controller...
							
							//or modify variables and track them like so

                            // Specify the property of the constant you wish to watch
							$scope.some_constant_setup_above = moduleNameConfig.constant1;
							// this watches the constant/value to see if it changes and when it does runs the function
							$scope.$watch('some_constant_setup_above', function(val) {
								// Determines if the argument is a string
                                if (angular.isString(val)) {
									// Converts the string to lowercase
                                    val = val.toLowerCase();
									// If the string is not empty run:
                                    if (val.length > 0) {
									    // Alert that the constant has changed
                                        $scope.some_constant_setup_above = "something you have modified. Oh or validated";
									}
                                }
                            });
							//when you do this the above watch will run
							$scope.some_constant_setup_above = "This can be changed / modified anywhere and the above function will validate";  
				}])
		.controller('moduleNameController2',['$scope', function($scope){
			//Look at the array. I passed '$scope' not $scope. Why??? So that the variables do not get lost when minified. 
			//You might also want to read this Read this https://docs.angularjs.org/guide/ie >>>> whacky ie8
			//You can have any number of controllers btw each doing a specific purpose
			//How i wish I had this 5 years ago
			//You can communicate from one controller to the next using services defined above. Feed them in and then set the vars and voila!!
		}]);
				
})();



//File Directive.js
(function() {
	'use strict';
	 angular.module('ui.modulename',[])
	 .directive('moduleNameDirective',['moduleNameConfig', '$moduleFactoryName', 'awholebunchofvars',
			  function( moduleNameConfig, $moduleFactoryName, awholebunchofvars) {
					return {
						restrict: 'EA',
						//Figure out what this really is .. it will help you a lot
						scope : {
							variable1: '=?',
							variable2: '=?somevar',
						    variable3: '=?',
						},
						controller: 'moduleNameController2', //link the controller here 
						link: function(scope, element, attrs) {
							 var callbacks = {
								accept: undefined,
								beforeDrag: undefined
							  };
							var config = {};
							angular.extend(config, moduleNameConfig);
							
							callbacks.unselect = function(node) {return true;}; //define the callbacks
							scope.$callbacks = callbacks;
						}
					};
			  }]);

})();