angular.module('profilesApp.profiles.service', [

])

// A RESTful factory for retrieving contacts from 'contacts.json'
.factory('profiles', ['$http', 'utils', function ($http, utils) {
  var path = 'assets/profiles.json';
  var profiles = $http.get(path).then(function (resp) {
    return resp.data.profiles;
  });

  var factory = {};
  factory.all = function () {
    return profiles;
  };
  factory.get = function (id) {
    return profiles.then(function(){
      return utils.findById(profiles, id);
    })
  };
  return factory;
}]);
